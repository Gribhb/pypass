import mysql.connector
import getpass
import re


mydb = mysql.connector.connect(
  host="172.60.0.2",
  user="pypass",
  password="pypasspasswd",
  database="pypass"
)

cursor = mydb.cursor()

def menu():
  global user_answer
  print("")
  print("-" * 65)
  user_answer = input(""" Select option :
  1: Add password
  2: Show password
  3: Remove password
  4: Quit
  Your choice : """)

# Check if masterpassword already exist
checkMasterPassword = "SHOW TABLES LIKE 'masterpasswordtest'"
cursor.execute(checkMasterPassword)
resultMasterPassword = cursor.fetchone()


# Set master password if he is not define, else access to your passwords registered or add new password
if resultMasterPassword:
  cursor = mydb.cursor()
  cursor.execute("SELECT masterpassword FROM masterpasswordtest")
  masterPassword = cursor.fetchone()
  masterPassword = masterPassword[0]

  print("Enter your masterpassword.")
  password = getpass.getpass('Password: ')

  trypass = 0
  while password != masterPassword:
    print("Authentification failed...")
    password = getpass.getpass('Password: ')
    trypass += 1
    if trypass >= 3:
      print("Too many auth... Bye")
      exit()

  cursor.execute("CREATE TABLE IF NOT EXISTS passwordstest (service VARCHAR(255), username VARCHAR(255), password VARCHAR(255))")

  menu()

  while user_answer != "4":
     if user_answer == "1":
       add_service = input("Enter service you want to add password: ")
       add_username = input("Enter username: ")
       add_pass = getpass.getpass("Enter password you want to add: ")
       
       sql = "INSERT INTO passwordstest (service, username, password) VALUES (%s, %s, %s)"
       val = (add_service, add_username, add_pass)
       cursor.execute(sql, val)
       mydb.commit()

       print(cursor.rowcount, "entry inserted.")

       menu()
     
     elif user_answer == "2":
       show_pass = input("Enter for which service you want to see your password: ")
       #cursor = mydb.cursor()
       cursor.execute("SELECT password FROM passwordstest WHERE service=%s", (show_pass,))
       show_pass= cursor.fetchone()
       print("This is your pass: ")
       print(show_pass[0])

       menu()
     
     elif user_answer == "3":
       del_pass = input("Enter for which service you want to delete: ")
       cursor.execute("DELETE FROM passwordstest WHERE service=%s", (del_pass,))
       mydb.commit()

       print(cursor.rowcount, 'entry deleted.')

       menu()
    
     elif user_answer == "4":
       print("Good bye!!")
       exit()

     else:
       print("")
       print("Please entre number between 1 and 4.")
       menu()

    
else:
#Les conditions sont les suivantes pour la conformité d'un mot de passe :
#  - Il doit avoir au minimum 8 caractères
#  - Il doit être composé au moins d'une majuscule
#  - Il doit être composé au moins d'une minuscule
#  - Il doit être composé au moins d'un caractère spécial

  chn_mdp = r"^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\W).{8,}$$"
  exp_mdp = re.compile(chn_mdp)
  
  setMasterPassword = ""
  while exp_mdp.search(setMasterPassword) is None:
    print("Please, define your masterpassword.")
    setMasterPassword = getpass.getpass('Password: ')
  
  cursor.execute("CREATE TABLE masterpasswordtest (name VARCHAR(255), masterpassword VARCHAR(255))")
  sql = "INSERT INTO masterpasswordtest (name, masterpassword) VALUES (%s, %s)"
  val = ("pypass", setMasterPassword)
  cursor.execute(sql, val)
  mydb.commit()

  print(cursor.rowcount, "masterpassword inserted.")

