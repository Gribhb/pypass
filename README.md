# pypass

Pypass is a password manager based on python script. He runs on docker environment.

*1 container is dedicated to the python script*

*1 container is dedicated to the database (mariadb)*

## Requirements

- You need docker installed

- Change network (if the actuel is in conflict with other containers)
```bash
docker/docker-compose.yml (line 14,34)
docker/app/pypass.py (line 7)
```

- Change mysql credentials
```bash
docker/env
docker/app/pypass.py
```

- Restrict permissions
```bash
cd docker
chmod -R 700 env
```

- Make executable install.sh
```bash
chmod +x install.sh
```
- Launch install.sh (build image + docker-compose up)
```bash
./install.sh
```

## Use pypass

```bash
docker exec -it pypass python3 pypass.py
```

The first use you need to set a masterpassword, then this password will be ask to access to the menu of pypass

## Pypass menu

1. Add password: enter the service for which you want to store password then username and password

2. Show password: enter the service and the password will be show to you

3. Remove password: entre the service for which you want to delete password

4. Quit: quit the script

